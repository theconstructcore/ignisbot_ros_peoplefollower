#!/usr/bin/env python3
import os

# ROS imports
import rospy
import rospkg


# trt_pose related imports
import json
import trt_pose.coco
import trt_pose.models
import torch
import torch2trt
from torch2trt import TRTModule
import time
import cv2
import torchvision.transforms as transforms
import PIL.Image
from trt_pose.draw_objects import DrawObjects
from trt_pose.parse_objects import ParseObjects
from jetcam.utils import bgr8_to_jpeg

class IgnisPoepleTracker(object):

    def __init__(self, do_benchmark=True, device_to_use="cuda", csi_camera=True):
        """
        device_to_use: Can be "cpu" or "cuda". If you dont have cuda, then use cpu.
        csi_camera: If true we conside camera images come from CSI, otherwise its a USB camera. Assuming 
        a Jetson Card is used.
        """
        rospy.loginfo("Starting IgnisPeopleTracker...")
        self._device_to_use = device_to_use
        
        rospy.loginfo("Getting Path to package...")
        follow_people_configfiles_path = rospkg.RosPack().get_path('ignisbot_people_follower_pkg')+"/trt_config_files"


        rospy.loginfo("We get the human pose json file that described the human pose") 
        humanPose_file_path = os.path.join(follow_people_configfiles_path, 'human_pose.json')

        rospy.loginfo("Opening json file")
        with open(humanPose_file_path, 'r') as f:
            human_pose = json.load(f)

        rospy.loginfo("Creating topology")
        topology = trt_pose.coco.coco_category_to_topology(human_pose)

        rospy.loginfo("# Load the model")
        num_parts = len(human_pose['keypoints'])
        num_links = len(human_pose['skeleton'])

        rospy.loginfo("Creating Model")
        model = trt_pose.models.resnet18_baseline_att(num_parts, 2 * num_links).cuda().eval()

        rospy.loginfo("Load the weights from the eight files predownloaded to this package")        
        MODEL_WEIGHTS = 'resnet18_baseline_att_224x224_A_epoch_249.pth'
        model_weights_path = os.path.join(follow_people_configfiles_path, MODEL_WEIGHTS)

        rospy.loginfo("Load state dict")   
        model.load_state_dict(torch.load(model_weights_path))

        WIDTH = 224
        HEIGHT = 224

        rospy.loginfo("Creating empty data")
        data = torch.zeros((1, 3, HEIGHT, WIDTH)).cuda()

        
        rospy.loginfo("Use tortchtrt to go from Torch to TensorRT to generate an optimised model")
        model_trt = torch2trt.torch2trt(model, [data], fp16_mode=True, max_workspace_size=1<<25)

        rospy.loginfo("We save that optimised model so that we dont have to do it everytime")
        OPTIMIZED_MODEL = 'resnet18_baseline_att_224x224_A_epoch_249_trt.pth'
        optimized_model_weights_path = os.path.join(follow_people_configfiles_path, OPTIMIZED_MODEL)

        rospy.loginfo("Saving new optimodel")
        torch.save(model_trt.state_dict(), optimized_model_weights_path)

        
        rospy.loginfo("We now load the saved model using Torchtrt")
        model_trt = TRTModule()
        model_trt.load_state_dict(torch.load(optimized_model_weights_path))

        
        rospy.loginfo("We benchmark the model")
        if do_benchmark:            

            t0 = time.time()
            torch.cuda.current_stream().synchronize()
            for i in range(50):
                y = model_trt(data)
            torch.cuda.current_stream().synchronize()
            t1 = time.time()

            print(50.0 / (t1 - t0))
        
        rospy.loginfo("Define the Image processing variables")
        mean = torch.Tensor([0.485, 0.456, 0.406]).cuda()
        std = torch.Tensor([0.229, 0.224, 0.225]).cuda()
        self.device = torch.device(self._device_to_use)

        rospy.loginfo("Clases to parse the obect of the NeuralNetwork and draw n the image")
        parse_objects = ParseObjects(topology)
        draw_objects = DrawObjects(topology)

        rospy.loginfo("Start setup camera")
        if csi_camera:
            from jetcam.csi_camera import CSICamera
            self.camera = CSICamera(width=WIDTH, height=HEIGHT, capture_fps=30)
        else:
            from jetcam.usb_camera import USBCamera
            self.camera = USBCamera(width=WIDTH, height=HEIGHT, capture_fps=30)

        rospy.loginfo("Starting IgnisPeopleTracker...DONE")

        
    def __del__(self):
        print("Terminating, deleting objects...")
        self.camera.unobserve_all()
        print("Terminating, deleting objects...Cleanup finished")

    def preprocess(self, image):
        self.device = torch.device(self._device_to_use)
        image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
        image = PIL.Image.fromarray(image)
        image = transforms.functional.to_tensor(image).to(device)
        image.sub_(mean[:, None, None]).div_(std[:, None, None])
        return image[None, ...]
    
    def execute(self, change):
        image = change['new']
        data = self.preprocess(image)
        cmap, paf = model_trt(data)
        cmap, paf = cmap.detach().cpu(), paf.detach().cpu()
        counts, objects, peaks = parse_objects(cmap, paf)#, cmap_threshold=0.15, link_threshold=0.15)
        draw_objects(image, counts, objects, peaks)
        image_w.value = bgr8_to_jpeg(image[:, ::-1, :])
    
    def start_peopletracker(self):
        rospy.loginfo("Starting Loop...")
        self.camera.running = True
        self.camera.observe(self.execute, names='value')
        rospy.spin()
        rospy.loginfo("Terminating Loop...")


if __name__ == "__main__":
    rospy.init_node("peopletracker_test_note", log_level=rospy.DEBUG)
    pt_object = IgnisPoepleTracker()
    pt_object.start_peopletracker()
